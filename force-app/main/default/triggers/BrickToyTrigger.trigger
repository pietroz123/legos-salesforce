trigger BrickToyTrigger on BrickToy__c(
    before insert,
    after insert,
    before update,
    after update,
    before delete,
    after delete,
    after undelete
) {
    new BrickToyTriggerHandler().run();
}
