public with sharing class BricksetAPIService {
    /**
     * @description Recupera as informações de um LEGO Set a partir do seu número
     * @author Pietro Zuntini Bonfim | 09-12-2021
     * @param setNumber
     * @return BricksetResponse
     **/
    public static BricksetResponse getSetsByNumber(String setNumber) {
        String apiKey = '3-Z9WS-kpLP-59cxU';
        String userHash = 'JRKsqSSmt4';
        String params = '{\'setNumber\':\'$num\'}'.replace('$num', setNumber);

        Http http = new Http();
        HttpRequest req = new HttpRequest();

        String endpoint = 'https://brickset.com/api/v3.asmx/getSets?apiKey=$apiKey&userHash=$userHash&params=$params'
            .replace('$apiKey', apiKey)
            .replace('$userHash', userHash)
            .replace('$params', params);

        req.setEndpoint(endpoint);

        req.setMethod('GET');

        HttpResponse res = http.send(req);
        System.debug(res.getBody());

        return BricksetResponse.parse(res.getBody());
    }

    /**
     * @description Recupera imagens adicionais de um LEGO
     * @author Pietro Zuntini Bonfim | 09-12-2021
     * @param setID
     * @return List<BricksetResponse.Image>
     **/
    public static List<BricksetResponse.Image> getAdditionalImages(String setID) {
        String apiKey = '3-Z9WS-kpLP-59cxU';

        Http http = new Http();
        HttpRequest req = new HttpRequest();

        String endpoint = 'https://brickset.com/api/v3.asmx/getAdditionalImages?apiKey=$apiKey&setID=$setID'
            .replace('$apiKey', apiKey)
            .replace('$setID', setID);

        req.setEndpoint(endpoint);

        req.setMethod('GET');

        HttpResponse res = http.send(req);
        System.debug(res.getBody());

        Map<String, Object> resMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        List<BricksetResponse.Image> images = (List<BricksetResponse.Image>) System.JSON.deserialize(
            JSON.serialize(resMap.get('additionalImages')),
            List<BricksetResponse.Image>.class
        );

        return images;
    }
}
