/***********************************************************************
 * @CreatedOn       : 12/09/2021
 * @Author          : Pietro Zuntini Bonfim
 * @Description     : Wrapper da reposta da API do Brickset
 *   - Documentação oficial: https://brickset.com/article/52664/api-version-3-documentation
 *
 * @LastUpdateBy    :
 ************************************************************************/
public with sharing class BricksetResponse {
    @AuraEnabled
    public String status;
    @AuraEnabled
    public Integer matches;
    @AuraEnabled
    public List<Sets> sets;

    public class Sets {
        @AuraEnabled
        public Integer setID;
        @AuraEnabled
        public String setNumber;
        @AuraEnabled
        public Integer numberVariant;
        @AuraEnabled
        public String name;
        @AuraEnabled
        public Integer year;
        @AuraEnabled
        public String theme;
        @AuraEnabled
        public String themeGroup;
        @AuraEnabled
        public String subtheme;
        @AuraEnabled
        public String category;
        // public released As Boolean
        @AuraEnabled
        public Integer pieces;
        @AuraEnabled
        public Integer minifigs;
        @AuraEnabled
        public Image image;
        @AuraEnabled
        public String bricksetURL;
        // public collection As New collection
        // public collections As New collections
        @AuraEnabled
        public LEGOCom LEGOCom;
        // public rating As Single
        // public reviewCount As integer
        // public packagingType As String
        // public availability As String
        @AuraEnabled
        public Integer instructionsCount;
        // public additionalImageCount As Integer
        // public ageRange As New ageRange
        // public dimensions As New dimensions
        // public barcode As New barcodes
        // public extendedData As New extendedData
        // public lastUpdated As DateTime
    }

    public class Image {
        @AuraEnabled
        public String thumbnailURL;
        @AuraEnabled
        public String imageURL;

        public Image(String imageURL, String thumbnailURL) {
            this.imageURL = imageURL;
            this.thumbnailURL = thumbnailURL;
        }
    }

    public class LEGOCom {
        @AuraEnabled
        public LEGOComDetails US;
        @AuraEnabled
        public LEGOComDetails UK;
        @AuraEnabled
        public LEGOComDetails CA;
        @AuraEnabled
        public LEGOComDetails DE;
    }

    public class LEGOComDetails {
        @AuraEnabled
        public Decimal retailPrice;
        @AuraEnabled
        public DateTime dateFirstAvailable;
        @AuraEnabled
        public DateTime dateLastAvailable;
    }

    public static BricksetResponse parse(String json) {
        return (BricksetResponse) System.JSON.deserialize(json, BricksetResponse.class);
    }

    public static BricksetResponse.Sets parseSets(String json) {
        return (BricksetResponse.Sets) System.JSON.deserialize(json, BricksetResponse.Sets.class);
    }
}
