/**
 * @description Demonstrates what a Service Layer object might look like
 * for the BrickToy__c object. Demonstrates the placement of shared code that
 * is specific to the BrickToy__c Object, and contains code that is called
 * by the BrickToyTriggerHandler
 * @see BrickToyTriggerHandler
 */
public with sharing class BrickToyServiceLayer {
    @testVisible
    private static String didExecuteMethod;

    /**
     * @description Internal custom exception class
     */
    public class BTSLException extends Exception {
    }

    /**
     * @description Responsável por preencher um LEGO com suas informações completas
     *  a partir da API do Brickset
     * @calledby BrickToyTriggerHandler
     * @event After Insert
     * @author Pietro Zuntini Bonfim | 09-12-2021
     * @param triggerNew
     **/
    public static void fillWithApiInformation(List<BrickToy__c> triggerNew) {
        for (BrickToy__c b : triggerNew) {
            if (b.Brand__c == 'LEGO') {
                fillWithApiInformationFuture(b.Id, b.SetNumber__c);
            }
        }
    }

    /**
     * @description Este método é necessário pois só é possível efetuar um callout no evento
     *  de After Insert a partir de um método future
     * @author Pietro Zuntini Bonfim | 09-12-2021
     * @param toyId
     * @param setNumber
     **/
    @AuraEnabled
    @future(callout=true)
    public static void fillWithApiInformationFuture(Id toyId, String setNumber) {
        List<BricksetResponse.Sets> sets = BricksetAPIService.getSetsByNumber(setNumber).sets;

        if (sets.isEmpty()) {
            return;
        }

        BricksetResponse.Sets legoSet = sets[0];

        // Preenche as informações
        BrickToy__c toy = new BrickToy__c(Id = toyId);
        toy.SetID__c = legoSet.setID;
        toy.NumberVariant__c = legoSet.numberVariant;
        toy.SetName__c = legoSet.name;
        toy.Year__c = legoSet.year;
        toy.Theme__c = legoSet.theme;
        toy.Pieces__c = legoSet.pieces;
        toy.NumberOfMinifigues__c = legoSet.minifigs;
        toy.BricksetURL__c = legoSet.bricksetURL;
        toy.USDRetailPrice__c = legoSet?.LEGOCom?.US?.retailPrice;
        toy.ImageURL__c = legoSet?.image?.imageURL;
        toy.ThumbnailURL__c = legoSet?.image?.thumbnailURL;
        update toy;
    }

    /**
     * @description Recupera as imagens de um LEGO Set para serem consumidas
     *   pelo componente legoSetAdditionalImages
     * @author Pietro Zuntini Bonfim | 09-12-2021
     * @param toyId
     * @return List<BricksetResponse.Image>
     **/
    @AuraEnabled
    public static List<BricksetResponse.Image> getLegoAdditionalImages(Id toyId) {
        try {
            BrickToy__c toy = [SELECT SetID__c, ImageURL__c, ThumbnailURL__c FROM BrickToy__c WHERE Id = :toyId];

            List<BricksetResponse.Image> images = new List<BricksetResponse.Image>();
            images.add(new BricksetResponse.Image(toy.ImageURL__c, toy.ThumbnailURL__c));

            images.addAll(BricksetAPIService.getAdditionalImages(String.valueOf(toy.SetID__c)));

            return images;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}
