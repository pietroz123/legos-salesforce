/**
 * @description Demonstrates how to construct a TriggerHandler using the
 * trigger handler framework found in Shared Code/TriggerHandler.cls
 * @group Trigger Recipes
 * @see TriggerHandler, BrickToyServiceLayer
 */
public with sharing class BrickToyTriggerHandler extends TriggerHandler {
    private List<BrickToy__c> triggerNew;
    private List<BrickToy__c> triggerOld;
    private Map<Id, BrickToy__c> triggerMapNew;
    private Map<Id, BrickToy__c> triggerMapOld;

    @TestVisible
    private static Exception circuitBreaker;

    /**
     * @description Custom exception class
     */
    public class BrickToyTriggerHandlerException extends Exception {
    }

    /**
     * @description Constructor that sets class variables based on Trigger
     * context vars
     */
    public BrickToyTriggerHandler() {
        this.triggerOld = (List<BrickToy__c>) Trigger.old;
        this.triggerNew = (List<BrickToy__c>) Trigger.new;
        this.triggerMapNew = (Map<Id, BrickToy__c>) Trigger.newMap;
        this.triggerMapOld = (Map<Id, BrickToy__c>) Trigger.oldMap;
    }

    // Insert Contexts

    /**
     * @description Before Insert context method. Called automatically by the
     * trigger framework this class extends.
     **/
    public override void beforeInsert() {
    }

    /**
     * @description after insert context method. Called automatically by the
     * trigger framework this class extends
     **/
    public override void afterInsert() {
        BrickToyServiceLayer.fillWithApiInformation(this.triggerNew);
    }

    // Update Contexts

    /**
     * @description before update context method. Called automatically by the
     * trigger framework this class extends
     **/
    public override void beforeUpdate() {
    }

    /**
     * @description after update context method. Called automatically by the
     * trigger framework this class extends
     **/
    public override void afterUpdate() {
    }

    // Delete Contexts

    /**
     * @description before delete context method. Called automatically by the
     * trigger framework this class extends
     **/
    public override void beforeDelete() {
    }

    /**
     * @description after delete context method. Called automatically by the
     * trigger framework this class extends
     **/
    public override void afterDelete() {
    }

    // Undelete Contexts

    /**
     * @description after undelete context method. Called automatically by the
     * trigger framework this class extends
     **/
    public override void afterUndelete() {
    }
}
