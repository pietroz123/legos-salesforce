import getLegoAdditionalImages from "@salesforce/apex/BrickToyServiceLayer.getLegoAdditionalImages";
import { api, LightningElement } from "lwc";

export default class LegoSetAdditionalImages extends LightningElement {
    @api recordId;
    loading = true;
    error = null;
    images = [];

    connectedCallback() {
        this.getImages();
    }

    getImages() {
        getLegoAdditionalImages({ toyId: this.recordId })
            .then((result) => {
                console.log("Result", result);
                this.images = result;
                this.loading = false;
                this.error = null;
            })
            .catch((error) => {
                console.error("Error:", error);
                this.loading = false;
                this.error = error;
            });
    }
}
