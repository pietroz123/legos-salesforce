import { LightningElement, api, wire } from "lwc";
import { getRecord, getFieldValue } from "lightning/uiRecordApi";
import { updateRecord } from "lightning/uiRecordApi";

import fillWithApiInformationFuture from "@salesforce/apex/BrickToyServiceLayer.fillWithApiInformationFuture";

import SET_NUMBER_FIELD from "@salesforce/schema/BrickToy__c.SetNumber__c";

const fields = [SET_NUMBER_FIELD];

export default class RefreshSetInformation extends LightningElement {
    isExecuting = false;
    @api recordId;
    @wire(getRecord, { recordId: "$recordId", fields })
    brickToy;

    @api invoke() {
        let setNumber = getFieldValue(this.brickToy.data, SET_NUMBER_FIELD);

        if (this.isExecuting) {
            return;
        }

        this.isExecuting = true;

        fillWithApiInformationFuture({ toyId: this.recordId, setNumber: setNumber })
            .then((res) => {
                console.log("🚀 / res", res);
                updateRecord({ fields: { Id: this.recordId } });
            })
            .catch((err) => {
                console.error("🚀 / err", err);
            })
            .finally(() => {
                this.isExecuting = false;
            });
    }
}
